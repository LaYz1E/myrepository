package app;

import app.domain.Person;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyApp {
    public static void main(String[] args) {
        Person person = new Person(13L, "Fedor");
        Person person1 = new Person(14L, "Petr");
        Person person2 = new Person(24L, "Nikolai");
        Logger.getGlobal().log(Level.INFO, Arrays.asList(person, person1, person2).toString());
    }
}
